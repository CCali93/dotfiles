# Dotfiles Repo
Just some dotfiles containing common configurations for linux systems to speed up machine setup

## Assumptions the Setup Script Makes
* git, curl, and tmux have been installed
* if they have not, be sure to install each of these before running `setup.sh`

## To setup:
Just run the `setup.sh` in the project root
