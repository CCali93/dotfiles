#!/bin/bash

#Install software
sudo apt-get install vim zsh

#copy over the important dotfiles
cp .vimrc ~
cp .tmux.conf ~

#Install all vim plugins
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim && \
    git clone git://github.com/tpope/vim-sensible.git ~/.vim/bundle/vim-sensible && \
    git clone git://github.com/jiangmiao/auto-pairs.git ~/.vim/bundle/auto-pairs

status=$?
if [$status -ne 0]; then
    exit $status
fi

#Setup Tmux Plugin Manager
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm && \
    tmux source-file ~/.tmux.conf

status=$?
if [$status -ne 0]; then
    exit $status
fi

#Install ZSH
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh

chsh -s `which zsh`

bold=$(tput bold)
normal=$(tput sgr0)

echo "${bold}To finish setup, do the following${normal}"
echo "* Run tmux and do Prefix+I to finalize plugin installation"
echo "* Once finished restart the computer with 'sudo shutdown -r 0'"
