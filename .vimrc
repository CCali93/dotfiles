execute pathogen#infect()
syntax on
filetype plugin indent on

set cc=80
set ts=4
set et
set sw=4

set foldenable
set foldlevelstart=10
set foldnestmax=10
